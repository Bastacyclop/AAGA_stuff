extern crate rand;

use rand::Rng;

use std::{mem, io};
use std::collections::VecDeque;

#[derive(Debug, Clone)]
pub struct Dag {
    succs: Vec<Vec<usize>>
}

#[derive(Clone, Copy)]
pub struct NodeId(usize);

impl Dag {
    pub fn new() -> Dag {
        Dag { succs: Vec::new() }
    }

    pub fn add_node(&mut self) -> NodeId {
        let i = self.succs.len();
        self.succs.push(Vec::new());
        NodeId(i)
    }

    // TODO: check cycles, check duplicates
    pub fn add_edge(&mut self, i: NodeId, j: NodeId) {
        self.succs[i.0].push(j.0);
    }

    fn compute_preds(&self) -> Vec<Vec<usize>> {
        let mut preds: Vec<Vec<_>> = (0..self.succs.len()).map(|_| Vec::new()).collect();
        for (i, succs) in self.succs.iter().enumerate() {
            for &succ in succs {
                preds[succ].push(i);
            }
        }
        preds
    }

    // TODO: complexity ??
    fn is_trans(succs: &[Vec<usize>], i: usize, j: usize) -> bool {
        i == j || succs[i].iter().any(|&s| Self::is_trans(succs, s, j))
    }

    pub fn is_cycle_free<F>(mut self, mut on_step: F) -> bool
        where F: FnMut(&Self, usize)
    {
        let mut preds = self.compute_preds();

        let mut todo: VecDeque<_> = (0..self.succs.len()).collect();
        let mut skips = 0;
        while skips < todo.len() {
            let i = todo.pop_front().unwrap();
            let ss = mem::replace(&mut self.succs[i], Vec::new());
            let ps = mem::replace(&mut preds[i], Vec::new());
            if (ss.len() == 0 && ps.len() == 1) ||
                (ps.len() == 0 && ss.len() == 1)
            {
                remove_in(&mut self.succs, &ps[..], i);
                remove_in(&mut preds, &ss[..], i);
                on_step(&self, i);
                skips = 0;
            } else if ss.len() == 1 && ps.len() == 1 {
                let p = ps[0];
                let s = ss[0];
                if Self::is_trans(&self.succs, p, s) {
                    self.succs[p].retain(|v| *v != i);
                    preds[s].retain(|v| *v != i);
                } else {
                    *self.succs[p].iter_mut().find(|v| **v == i).unwrap() = s;
                    *preds[s].iter_mut().find(|v| **v == i).unwrap() = p;
                }
                on_step(&self, i);
                skips = 0;
            } else {
                mem::replace(&mut self.succs[i], ss);
                mem::replace(&mut preds[i], ps);
                todo.push_back(i);
                skips += 1;
            }
        }
        todo.len() == 1
    }

    pub fn graphviz<W, H>(&self, w: &mut W, header: H) -> io::Result<()>
        where W: io::Write,
              H: FnOnce(&mut W) -> io::Result<()>,
    {
        writeln!(w, "digraph G {{")?;
        header(w)?;
        for (i, ss) in self.succs.iter().enumerate() {
            for j in ss {
                writeln!(w, "n{} -> n{};", i, j)?;
            }
        }
        writeln!(w, "}}")
    }

    fn next_cycle_frees<F>(&self, mut f: F)
        where F: FnMut(Self)
    {
        let mut trans = Vec::new();
        for (a, ss) in self.succs.iter().enumerate() {
            {
                let mut d = self.clone();
                let top = d.add_node();
                d.add_edge(top, NodeId(a));
                f(d);
            }
            {
                let mut d = self.clone();
                let bot = d.add_node();
                d.add_edge(NodeId(a), bot);
                f(d);
            }
            for (i, &b) in ss.iter().enumerate() {
                let mut d = self.clone();
                let intermediate = d.add_node();
                d.succs[a][i] = intermediate.0;
                d.add_edge(intermediate, NodeId(b));
                f(d);

                trans.push((a, b));
            }
        }

        while let Some((a, b)) = trans.pop() {
            for &c in &self.succs[b] {
                let mut d = self.clone();
                let intermediate = d.add_node();
                d.add_edge(NodeId(a), intermediate);
                d.add_edge(intermediate, NodeId(c));
                f(d);

                trans.push((a, c));
            }
        }
    }

    fn next_cycle_frees_nodup(&self, res: &mut Vec<Self>) {
        self.next_cycle_frees(|d| {
            if res.iter().all(|nd| nd != &d) {
                res.push(d);
            }
        });
    }

    pub fn gen_cycle_free<R: Rng, F>(rng: &mut R, n: usize, mut f: F) -> Self
        where F: FnMut(usize, usize, &Self)
    {
        let mut dag = Self::new();
        dag.add_node();
        let mut next = Vec::new();

        for i in 1..n {
            dag.next_cycle_frees_nodup(&mut next);
            let choice = rng.gen_range(0, next.len());
            dag = next.swap_remove(choice);
            f(i + 1, next.len() + 1, &dag);
            next.clear();
        }

        dag
    }

    pub fn all_cycle_free<F>(n: usize, mut f: F) -> Vec<Self>
        where F: FnMut(&[Self])
    {
        let mut dag = Self::new();
        dag.add_node();
        let mut prev = vec![dag];
        let mut next = Vec::new();

        for _ in 1..n {
            for p in &prev {
                p.next_cycle_frees_nodup(&mut next);
            }
            f(&next);
            mem::swap(&mut prev, &mut next);
            next.clear();
        }

        prev
    }

    fn eq(&self, other: &Dag,
          s_tops: &[usize],
          o_tops: &[usize]) -> bool
    {
        if self.succs.len() != other.succs.len() {
            return false;
        }

        fn biject_level<'a>(
            ss: &'a [Vec<usize>], os: &'a [Vec<usize>],
            level: &[(&'a [usize], &'a [usize])],
            next: &mut Vec<(&'a [usize], &'a [usize])>,
            b: Bijection
        ) -> Option<Bijection>
        {
            // println!("level: {:?} {:?}", level, next);
            if let Some((&(sns, ons), level)) = level.split_first() {
                if sns.len() != ons.len() {
                    None
                } else {
                    biject(ss, os, level, next, sns, ons, b)
                }
            } else {
                if next.is_empty() {
                    Some(b)
                } else {
                    let level = next;
                    let next = &mut Vec::new();
                    biject_level(ss, os, level, next, b)
                }
            }
        }

        fn biject<'a>(
            ss: &'a [Vec<usize>], os: &'a [Vec<usize>],
            level: &[(&'a [usize], &'a [usize])],
            next: &mut Vec<(&'a [usize], &'a [usize])>,
            sns: &[usize], ons: &[usize],
            b: Bijection,
        ) -> Option<Bijection>
        {
            if let Some((&sn, sns)) = sns.split_first() {
                for &on in ons {
                    // println!("{:?}", b);
                    // println!("try {} <-> {} ({:?}, {:?})", sn, on, sns, ons);
                    next.push((&ss[sn], &os[on]));
                    let b = b.set(sn, on)
                        .and_then(|b| biject(ss, os, level, next, sns, ons, b));
                    if b.is_some() { return b; }
                    next.pop();
                }

                None
            } else {
                biject_level(ss, os, level, next, b)
            }
        }

        let level = &mut vec![(s_tops, o_tops)];
        let next = &mut Vec::new();
        let b = Bijection::new(self.succs.len());
        // println!("---- tops: {:?} {:?}", s_tops, o_tops);
        biject_level(&self.succs, &other.succs, level, next, b).is_some()
    }
    
    fn tops(&self) -> Vec<usize> {
        empty_indices(&self.compute_preds())
    }
}

fn empty_indices<T>(vs: &[Vec<T>]) -> Vec<usize> {
    vs.iter().enumerate()
        .filter(|&(_, v)| v.is_empty())
        .map(|(i, _)| i)
        .collect()
}

impl PartialEq for Dag {
    fn eq(&self, other: &Dag) -> bool {
        let st = self.tops();
        let ot = other.tops();
        Dag::eq(self, other, &st, &ot)
    }
}

fn remove_in(index_to_list: &mut [Vec<usize>], indices: &[usize], value: usize) {
    for &i in indices {
        let list = &mut index_to_list[i];
        list.retain(|v| v != &value);
    }
}

#[derive(Debug, Clone)]
struct Bijection {
    left: Vec<Option<usize>>,
    right: Vec<Option<usize>>,
}

impl Bijection {
    fn new(len: usize) -> Self {
        let right: Vec<_> = (0..len).map(|_| None).collect();
        Bijection {
            left: right.clone(),
            right,
        }
    }

    fn set(&self, l: usize, r: usize) -> Option<Self> {
        let ok =
            self.left[l].map_or(true, |r2| r == r2) &&
            self.right[r].map_or(true, |l2| l == l2);
        if ok {
            let mut b = self.clone();
            b.left[l] = Some(r);
            b.right[r] = Some(l);
            Some(b)
        } else {
            None
        }
    }
}

#[macro_export]
macro_rules! add_nodes {
    ($dag:ident, $($n:ident),*) => {
        $(let $n = $dag.add_node();)*
    }
}

#[macro_export]
macro_rules! add_edges {
    ($dag:ident, $(($a:ident, $b:ident)),*) => {
        $($dag.add_edge($a, $b);)*
    }
}

#[cfg(test)]
mod tests {
    use super::Dag;

    #[test]
    fn simple_cycle_free() {
        let mut dag = Dag::new();
        add_nodes!(dag, a, b, c, d, e, f);
        add_edges!(dag, (a, b), (a, c), (b, d), (c, d), (c, e), (d, f));
        assert!(dag.is_cycle_free(|_, _| {}));
    }

    #[test]
    fn simple_not_cycle_free() {
        let mut dag = Dag::new();
        add_nodes!(dag, a, b, c, d, e, f);
        add_edges!(dag, (a, b), (a, c), (b, d), (c, d), (c, e), (d, f), (b, e));
        assert_eq!(dag.is_cycle_free(|_, _| {}), false);
    }

    #[test]
    fn simple_eq() {
        let mut da = Dag::new();
        add_nodes!(da, a, b, c, d, e, f);
        add_edges!(da, (a, b), (a, c), (b, d), (c, d), (c, e), (d, f));

        let mut db = Dag::new();
        add_nodes!(db, g, h, i, j, k, l);
        add_edges!(db, (l, k), (l, j), (k, i), (j, i), (j, h), (i, g));

        assert_eq!(da, db);
    }

    #[test]
    fn simple_neq() {
        let mut da = Dag::new();
        add_nodes!(da, a, b, c, d, e, f);
        add_edges!(da, (a, b), (a, c), (b, d), (c, d), (c, e), (d, f));

        let mut db = Dag::new();
        add_nodes!(db, h, i, j, k, l);
        add_edges!(db, (l, k), (l, j), (k, i), (j, i), (j, h));

        assert_ne!(da, db);
    }

    #[test]
    fn eq_neq_3() {
        let mut d1 = Dag::new();
        add_nodes!(d1, n0, n1, n2);
        let mut d2 = d1.clone();
        let mut d3 = d1.clone();
        let mut d4 = d1.clone();

        add_edges!(d1, (n1, n0), (n2, n0));
        add_edges!(d2, (n1, n0), (n0, n2));
        add_edges!(d3, (n2, n1), (n1, n0));
        add_edges!(d4, (n1, n0), (n1, n2));

        assert_eq!(d1, d1);
        assert_eq!(d2, d2);
        assert_eq!(d3, d3);
        assert_eq!(d2, d3);
        assert_eq!(d4, d4);

        assert_ne!(d1, d2);
        assert_ne!(d1, d3);
        assert_ne!(d1, d4);
        assert_ne!(d2, d4);

        let mut d5 = d1.clone();
        add_edges!(d5, (n1, n2));
        assert_ne!(d1, d5);
        assert_ne!(d2, d5);
        assert_ne!(d3, d5);
        assert_ne!(d4, d5);
    }

    #[test]
    fn eq_4() {
        let mut da = Dag::new();
        add_nodes!(da, n0, n1, n2, n3);
        let mut db = da.clone();
        add_edges!(da, (n2, n0), (n1, n0), (n1, n3));
        add_edges!(db, (n1, n0), (n2, n0), (n2, n3));
        assert_eq!(da, db);
    }

    #[test]
    fn eq_5() {
        let mut da = Dag::new();
        add_nodes!(da, n0, n1, n2, n3, n4);
        let mut db = da.clone();

        add_edges!(da, (n1, n0), (n1, n2), (n1, n3), (n4, n3));
        add_edges!(db, (n1, n0), (n1, n2), (n1, n3), (n4, n2));
        assert_eq!(da, db);
    }

    #[test]
    fn weird_5() {
        let a1 = Dag { succs: vec![vec![3], vec![0], vec![0, 4], vec![], vec![3]] };
        let a2 = Dag { succs: vec![vec![2], vec![0, 3], vec![], vec![2], vec![3]] };
        assert_eq!(a1, a2);
        assert_eq!(a2, a1);

        let b1 = Dag { succs: vec![vec![], vec![0, 4], vec![0], vec![1], vec![]] };
        let b2 = Dag { succs: vec![vec![2, 3], vec![0], vec![], vec![], vec![3]] };
        assert_eq!(b1, b2);
        assert_eq!(b2, b1);

        let c1 = Dag { succs: vec![vec![2], vec![0, 3, 4], vec![], vec![0], vec![2]] };
        let c2 = Dag { succs: vec![vec![2], vec![0, 3, 4], vec![], vec![2], vec![3]] };
        assert_eq!(c1, c2);
        assert_eq!(c2, c1);

        let d1 = Dag { succs: vec![vec![], vec![0, 2, 3, 4], vec![0], vec![2], vec![0]] };
        let d2 = Dag { succs: vec![vec![], vec![0, 2, 3, 4], vec![0], vec![0], vec![3]] };
        assert_eq!(d1, d2);
        assert_eq!(d2, d1);
    }
}