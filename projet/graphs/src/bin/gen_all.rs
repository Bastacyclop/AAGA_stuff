extern crate graphs;

use std::{io, fs, env};
use std::path::Path;
use std::process::Command;
use graphs::*;

fn main() {
    let count = env::args().skip(1).next()
        .unwrap_or_else(|| String::from("6"));
    let c: usize = count.parse().unwrap();

    let out = Path::new("output")
        .join(format!("gen-all-{}", count));
    println!("Preparing {:?} directory", out);
    let _ = fs::remove_dir_all(&out);
    fs::create_dir_all(&out).unwrap();

    let mut n = 1;
    let mut tmp = String::new();
    println!("generating all graphs of size {}", c);
    Dag::all_cycle_free(c, |dags| {
        use std::fmt::Write;

        n += 1;
        for (i, dag) in dags.iter().enumerate() {
            write!(&mut tmp, "{}_{}", n, i).unwrap();
            let p = out.join(&tmp).with_extension("gv");
            tmp.clear();

            println!("Generating {:?}", p);
            let f = fs::File::create(p).unwrap();
            let w = &mut io::BufWriter::new(f);
            dag.graphviz(w, |_| Ok(())).unwrap();
        }
    });

    println!("Generating pngs with graphviz");
    Command::new("find")
        .arg(out)
        .arg("-name")
        .arg("*.gv")
        .arg("-execdir")
        .arg("dot")
        .arg("-Tpng")
        .arg("-o")
        .arg("{}.png")
        .arg("{}")
        .arg(";")
        .status()
        .unwrap();
}