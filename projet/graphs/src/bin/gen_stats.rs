extern crate graphs;
extern crate rand;

use graphs::*;

fn main() {
    let n = 6;
    let samples: u32 = 20_000;
    let rng = &mut rand::thread_rng();

    println!("generating all graphs of size {}", n);
    let mut i = 1;
    let all_dags = Dag::all_cycle_free(n, |_| {
        i += 1;
        println!("n = {}", i);
    });
    println!("got {} of them", all_dags.len());

    let mut dups = Vec::new();
    for (a, da) in all_dags.iter().enumerate() {
        for (b, db) in all_dags.iter().enumerate().skip(a+1) {
            let e1 = da == db;
            let e2 = db == da;
            if e1 || e2 {
                dups.push((a, b, e1, e2));
            }
        }
    }
    if dups.len() > 0 {
        for (a, b, e1, e2) in dups {
            println!("{}: {:?}", a, all_dags[a]);
            println!("{}: {:?}", b, all_dags[b]);
            println!("{} / {}", e1, e2);
        }
        panic!("dups");
    }

    println!("generating {} random samples", samples);
    let mut counts: Vec<u32> = all_dags.iter().map(|_| 0).collect();
    for _ in 0..samples {
        let dag = Dag::gen_cycle_free(rng, n, |_, _, _| {});
        let i = all_dags.iter().position(|d| &dag == d)
            .unwrap_or_else(|| panic!("{:?} not in all dags", dag));
        counts[i] += 1;
    }
    
    let expected = 1.0 / all_dags.len() as f32;
    let mut min = 1. / 0.;
    let mut max = 0.;
    let mut error_sum = 0.;
    for &c in &counts {
        let p = c as f32 / samples as f32;
        if p < min { min = p; }
        if p > max { max = p; }
        error_sum += (p - expected).abs();
    }

    println!("expected: {}", expected);
    println!("mean error: {}", error_sum / counts.len() as f32);
    println!("min: {}", min);
    println!("max: {}", max);
}