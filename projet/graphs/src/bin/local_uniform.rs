extern crate graphs;
extern crate rand;

use std::{io, fs, env};
use std::path::Path;
use std::process::Command;
use graphs::*;

fn main() {
    let count = env::args().skip(1).next()
        .unwrap_or_else(|| String::from("20"));
    let n: usize = count.parse().unwrap();

    let out = Path::new("output").join("local_uniform");
    println!("Preparing {:?} directory", out);
    fs::create_dir_all(&out).unwrap();

    let dag = Dag::gen_cycle_free(&mut rand::thread_rng(), n, |i, count, _| {
        println!("n = {}, {} choices", i, count);
    });
    if !dag.clone().is_cycle_free(|_, _| {}) {
        panic!("Generated graph is not cycle free :(");
    }

    let p = out.join(count).with_extension("gv");
    println!("Generating {:?}", p);
    {
        let f = fs::File::create(&p).unwrap();
        let w = &mut io::BufWriter::new(f);
        dag.graphviz(w, |_| Ok(())).unwrap();
    }

    println!("Generating pngs with graphviz");
    Command::new("find")
        .arg(out)
        .arg("-name")
        .arg("*.gv")
        .arg("-execdir")
        .arg("dot")
        .arg("-Tpng")
        .arg("-o")
        .arg("{}.png")
        .arg("{}")
        .arg(";")
        .status()
        .unwrap();
}