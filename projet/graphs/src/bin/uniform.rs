extern crate graphs;
extern crate rand;

use std::{io, fs, env};
use std::path::Path;
use std::process::Command;
use rand::Rng;
use graphs::*;

fn main() {
    let count = env::args().skip(1).next()
        .unwrap_or_else(|| String::from("6"));
    let c: usize = count.parse().unwrap();

    let out = Path::new("output").join("uniform");
    println!("preparing {:?} directory", out);
    fs::create_dir_all(&out).unwrap();

    let mut n = 1;
    println!("generating all graphs of size {}", c);
    let mut all_dags = Dag::all_cycle_free(c, |dags| {
        n += 1;
        println!("n = {}, {} dags", n, dags.len());
    });

    let rng = &mut rand::thread_rng();
    let choice = rng.gen_range(0, all_dags.len());
    let dag = all_dags.swap_remove(choice);

    let p = out.join(count).with_extension("gv");
    println!("generating {:?}", p);
    {
        let f = fs::File::create(&p).unwrap();
        let w = &mut io::BufWriter::new(f);
        dag.graphviz(w, |_| Ok(())).unwrap();
    }

    println!("generating pngs with graphviz");
    Command::new("find")
        .arg(out)
        .arg("-name")
        .arg("*.gv")
        .arg("-execdir")
        .arg("dot")
        .arg("-Tpng")
        .arg("-o")
        .arg("{}.png")
        .arg("{}")
        .arg(";")
        .status()
        .unwrap();
}