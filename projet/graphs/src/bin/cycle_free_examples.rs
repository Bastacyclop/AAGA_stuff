#[macro_use]
extern crate graphs;

use std::{io, fs};
use std::path::Path;
use std::process::Command;
use graphs::*;

fn graphviz_cycle_check(name: &str, dag: Dag) {
    use io::Write;

    let out = Path::new("output").join(name);
    println!("Preparing {:?} directory", out);
    let _ = fs::remove_dir_all(&out);
    fs::create_dir_all(&out).unwrap();

    let mut step = 0;
    let mut tmp = String::new();
    let writer = &mut || {
        use std::fmt::Write;

        write!(&mut tmp, "{}", step).unwrap();
        let p = out.join(&tmp).with_extension("gv");
        tmp.clear();
        step += 1;

        println!("Generating {:?}", p);
        let f = fs::File::create(p).unwrap();
        io::BufWriter::new(f)
    };

    dag.graphviz(&mut writer(), |_| Ok(())).unwrap();
    let result = dag.is_cycle_free(|d, target| {
        d.graphviz(&mut writer(), |w| {
            writeln!(w, "n{} [color=red];", target)
        }).unwrap();
    });

    println!("Generating pngs with graphviz");
    Command::new("find")
        .arg(&out)
        .arg("-name")
        .arg("*.gv")
        .arg("-execdir")
        .arg("dot")
        .arg("-Tpng")
        .arg("-o")
        .arg("{}.png")
        .arg("{}")
        .arg(";")
        .status()
        .unwrap();

    println!("--> cycle free? {}", result);
}

fn main() {
    let mut dag = Dag::new();
    add_nodes!(dag, n0, n1, n2, n3, n4, n5, n6,
                    n7, n8, n9, n10, n11, n12);
    add_edges!(dag, (n0, n1), (n0, n2),
                    (n1, n3), (n2, n3), (n2, n4),
                    (n3, n5), (n6, n2), (n9, n4),
                    (n4, n7), (n4, n8), (n8, n12),
                    (n0, n10), (n10, n11), (n11, n5));
    graphviz_cycle_check("cycle-free", dag.clone());

    add_edges!(dag, (n1, n4));
    graphviz_cycle_check("not-cycle-free", dag);
}

