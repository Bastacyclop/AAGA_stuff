# Intructions

- Lancer avec `cargo run --release` pour obtenir les données (créé `bench/data`)
- `cd bench && gnuplot plots` pour obtenir les graphes en .png
- Tester les tri avec `cargo test --release`
