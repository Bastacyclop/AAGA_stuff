use std::{ io, fs };
use std::path::Path;

extern crate time;

mod shuffle;
mod quicksort;

fn main() {
    use io::Write;
    let out = Path::new("bench");
    if fs::create_dir_all(&out).is_ok() {
        println!("Created {:?} directory", out);
    }
    let mut f = fs::File::create(out.join("data")).unwrap();

    let ns = 14;
    let mut n = 8;
    let mut samples = 2usize.pow(ns + 1);
    for _ in 0..ns {
        let mut permuts: Vec<_> = (0..samples)
            .map(|_| shuffle::generate(n))
            .collect();

        let start = time::precise_time_ns();
        for p in &permuts { quicksort::copy(p); }
        let cp_time = (time::precise_time_ns() - start) as f32;
        let cp_avg = cp_time / (samples as f32);

        let start = time::precise_time_ns();
        for p in &mut permuts { quicksort::in_place(p); }
        let ip_time = (time::precise_time_ns() - start) as f32;
        let ip_avg = ip_time / (samples as f32);

        writeln!(&mut f, "{} {} {}", n, cp_avg, ip_avg).unwrap();
        println!("{} {}", n, samples);

        n *= 2;
        samples /= 2;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sort_in_place_test() {
        let nb = 100_000;
        println!("Generating and sort {} permutations of size 8", nb);
        for _ in 1..nb {
            let mut gt = shuffle::generate(80);
            quicksort::in_place(&mut gt[..]);
            assert!(quicksort::sorted(&gt[..]));
        }
    }

    #[test]
    fn sort_copy_test() {
        let nb = 100_000;
        println!("Generating and sort {} permutations of size 8", nb);
        for _ in 1..nb {
            let mut gt = shuffle::generate(80);
            gt = quicksort::copy(&gt[..]);
            assert!(quicksort::sorted(&gt[..]));
        }
    }

}
