use std::cmp::PartialOrd;

pub fn sorted<T : PartialOrd>(t : &[T]) -> bool {
    t.iter().zip(t.iter().skip(1))
        .all(|(x1, x2)| { x1 < x2 })
}

pub fn copy<T : PartialOrd + Copy>(t : &[T]) -> Vec<T> {
    if t.len() <= 1 {
        return t.to_vec();
    }
    let pivot = t[0];
    let mut tmp = Vec::with_capacity(t.len());
    tmp.push(pivot);
    let mut p = 0;
    for i in 1..t.len() {
        if t[i] < pivot {
            tmp.insert(p, t[i]);
            p+=1;
        } else {
            tmp.push(t[i]);
        }
    }
    let mut res = copy(&tmp[..p]);
    res.push(pivot);
    res.append(&mut copy(&tmp[p+1..]));
    res
}

pub fn in_place<T : PartialOrd>(t : &mut [T]) {
    if t.len() <= 1 { return }

    let mut i = 0;
    for j in 1..t.len() {
        if t[j] < t[0] {
            i+=1;
            t.swap(i, j);
        }
    }
    if t[i] < t[0] {
        t.swap(i, 0);
    }
    let p = i;

    in_place(&mut t[..p]);
    in_place(&mut t[p+1..])
}
