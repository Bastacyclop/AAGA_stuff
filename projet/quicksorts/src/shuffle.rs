extern crate rand;

use self::rand::Rng;

pub fn generate(n: usize) -> Vec<usize> {
    let mut rng = rand::thread_rng();
    let mut t: Vec<usize> = Vec::with_capacity(n+1);
    for i in 0..n {
        t.push(i + 1);
    }
    for i in (0..n).rev() {
        let n: usize = rng.gen_range(0, i+1);
        t.swap(n,i);
    }
    t
}

/*
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn gen_test() {
        let nb = 1_000_000;
        println!("Generating {} permutations of size 8", nb);
        let mut t: Vec<f32> = Vec::with_capacity(9);
        for _ in 0..8 {
            t.push(0.0);
        }
        for _ in 1..nb {
            let gt = generate(8);
            for i in 0..8 {
                t[i]+=gt[i] as f32;
            }
        }
        for i in 0..8 {
            t[i]/=nb as f32;
        }
        println!("Average : {:?}", t);
    }

}
*/
