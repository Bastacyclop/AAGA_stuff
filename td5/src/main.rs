extern crate rand;

use rand::Rng;

fn gen_count(n: usize) -> Vec<usize> {
    let mut counts : Vec<usize> = Vec::with_capacity(n+1);
    counts.push(0);
    counts.push(1);
    for i in 2..n+1 {
        let mut acc = 0;
        for k in 1..i {
            acc += counts[k] * counts[i-k];
        }
        counts.push(acc);
    }
    counts
}

#[derive(Debug)]
enum Node {
    Internal,
    External(usize, usize),
}

#[derive(Debug)]
struct Tree(Vec<Node>);

fn gen(n: usize) -> Tree {
    let counts = gen_count(n);
    let mut nodes = Vec::with_capacity(n);
    let mut rng = rand::thread_rng();
    
    fn iter<R>(counts: &[usize],
               nodes: &mut Vec<Node>,
               rng: &mut R,
               n : usize)
        where R : Rng
    {
        if n==1 {
            nodes.push(Node::Internal);
            return
        }
        let mut nb = rng.gen_range(0, counts[n]);
        for k in 1..n {
            if nb < counts[k] * counts[n-k] {
                let i = nodes.len();
                nodes.push(Node::Internal);
                let il = nodes.len();
                iter(counts, nodes, rng, k);
                let ir = nodes.len();
                iter(counts, nodes, rng, n-k);
                nodes[i] = Node::External(il, ir);
                return
            }
            println!("ck : {:?}", counts[k]);
            println!("cn-k : {:?}", counts[n-k]);
            println!("c.len {:?} k{:?} n-k{:?}", counts.len(), k, n-k);
            println!("ck * cn-k : {:?}",counts[k] * counts[n-k]);
            nb -= counts[k] * counts[n-k];
        }
        unreachable!();
    }
    iter(&counts, &mut nodes, &mut rng, n);
    Tree(nodes)
}

fn main() {
    println!("{:?}", gen_count(8));
    println!("{:?}", gen(8));
}
