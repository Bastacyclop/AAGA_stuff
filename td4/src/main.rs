extern crate rand;

use std::{mem, cmp};
use std::time::Instant;
use rand::Rng;

#[derive(Debug)]
struct Node {
    left: Option<usize>,
    right: Option<usize>,
    parent: Option<usize>,
}

impl Node {
    fn leaf(parent: Option<usize>) -> Node {
        Node {
            left: None,
            right: None,
            parent,
        }
    }
}

#[derive(Debug)]
struct Tree(Vec<Node>);

impl Tree {
    fn generate(n: usize) -> Tree {
        let mut rng = rand::thread_rng();

        let mut nodes = vec![Node::leaf(None)];
        let mut leaves = vec![0];
        let mut unaries = Vec::new();
        let mut binaries = Vec::new();

        for _ in 0..n {
            let leaf_weight = 3;
            let unary_weight = 2;
            let binary_weight = 1;

            let leaves_weight = leaves.len() * leaf_weight;
            let unaries_weight = unaries.len() * unary_weight;
            let binaries_weight = binaries.len() * binary_weight;

            let total_weight = leaves_weight + unaries_weight + binaries_weight;
            let mut i = rng.gen_range(0, total_weight);
            let next_i = nodes.len();

            enum Target { Leaf(usize), Unary(usize), Binary(usize) }
            let target = if i < leaves_weight {
                i /= leaf_weight;
                Target::Leaf(leaves[i])
            } else {
                i -= leaves_weight;
                if i < unaries_weight {
                    i /= unary_weight;
                    Target::Unary(unaries[i])
                } else {
                    i -= unaries_weight;
                    Target::Binary(binaries[i])
                }
            };

            match target {
                Target::Leaf(target) => {
                    mem::replace(if rng.gen() {
                        &mut nodes[target].left
                    } else {
                        &mut nodes[target].right
                    }, Some(next_i));
                    unaries.push(target);
                    leaves[i] = next_i;
                    nodes.push(Node::leaf(Some(target)));
                }
                Target::Unary(target) => if rng.gen() {
                    mem::replace(match &mut nodes[target] {
                        &mut Node { left: ref mut t @ None, .. } |
                        &mut Node { right: ref mut t @ None, .. } => t,
                        _ => unreachable!()
                    }, Some(next_i));
                    binaries.push(target);
                    unaries.swap_remove(i);
                    leaves.push(next_i);
                    nodes.push(Node::leaf(Some(target)));
                } else {
                    let parent = nodes[target].parent;
                    let n = mem::replace(&mut nodes[target], if rng.gen() {
                        Node { left: Some(next_i), right: None, parent }
                    } else {
                        Node { left: None, right: Some(next_i), parent }
                    });
                    unaries.push(next_i);
                    nodes.push(Node { parent: Some(target), .. n });
                }
                Target::Binary(target) => {
                    let parent = nodes[target].parent;
                    let n = mem::replace(&mut nodes[target], if rng.gen() {
                        Node { left: Some(next_i), right: None, parent }
                    } else {
                        Node { left: None, right: Some(next_i), parent }
                    });
                    unaries.push(target);
                    binaries[i] = next_i;
                    nodes.push(Node { parent: Some(target), .. n });
                }
            }
        }

        Tree(nodes)
    }

    fn depth(&self) -> u32 {
        fn iter(nodes: &[Node], i: usize) -> u32 {
            let n = &nodes[i];
            1 + cmp::max(n.left.map(|l| iter(nodes, l)).unwrap_or(0),
                         n.right.map(|r| iter(nodes, r)).unwrap_or(0))
        }

        let &Tree(ref nodes) = self;
        if nodes.is_empty() { 0 } else { iter(nodes, 0) }
    }
}

fn main() {
    let samples = 100;
    println!("samples = {}", samples);
    for n in 0..7 {
        let size = 10usize.pow(n);
        let mut depth = 0.0;
        let start = Instant::now();
        for _ in 0..samples {
            let t = Tree::generate(size);
            depth += t.depth() as f32;
        }
        let avg_depth = depth / samples as f32;
        let duration = start.elapsed();
        let msecs = duration.as_secs() as f64 * 1e3
           + duration.subsec_nanos() as f64 * 1e-6;
        let avg_time = msecs / samples as f64;
        println!("size = {}, avg depth = {}, avg time = {:.4} ms", size, avg_depth, avg_time);
    }
}
